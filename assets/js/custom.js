/*Global Variables*/
var Winwidth, Winheight, DeviceType, CurrentDevice, MobileMenu, ResizeTimer, CurrentBreakpointm;

var ezone = {} || ezone;

winWidth = window.innerWidth;

if (typeof ezone.commonFunctions === 'undefined') {
	var commonFunctions = {};
}

(function ($) {

	"use strict";

	ezone.commonFunctions = {
		
		
		UserAgentCode : function () {
			// detect touch
			if (navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) 
				|| navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i)
				|| navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/)
				|| navigator.userAgent.match(/Windows Phone/i) || navigator.userAgent.match(/ZuneWP7/i)) {
				$('body').addClass('touch-screen');
			}
		},

		scrollTarget: function (target, el, val) {
            el = el || $('html, body');
            val = val || 0;
            var elScrollToTopVal = el.scrollTop();
            var targetOffsetVal = target.offset().top - val;
            el.animate({
                scrollTop: targetOffsetVal
            }, 'slow');
        },
		
		hemMenu : function() {
			 $(".hamburger").on('click', function(e){ 
	                 e.preventDefault();
	                 $(this).toggleClass("open");
	                 $('body').toggleClass('offcanvas');
	                 $('#header-navigation').slideToggle();
	             });
		},

		loginPopup : function() {

			$('#login-form-link').click(function(e) {
				$("#login-form").delay(100).fadeIn(100);
		 		$("#register-form").fadeOut(100);
				$('#register-form-link').removeClass('active');
				$(this).addClass('active');
				e.preventDefault();
			});
			$('#register-form-link').click(function(e) {
				$("#register-form").delay(100).fadeIn(100);
		 		$("#login-form").fadeOut(100);
				$('#login-form-link').removeClass('active');
				$(this).addClass('active');
				e.preventDefault();
			});
		},

		homepageAudio : function() {

			if($('#audio').length > 0){
				audiooption.onclick = function(e) {
				  e.preventDefault();

				  var elm = e.target;
				  var audio = document.getElementById('audio');

				  var source = document.getElementById('audioSource');
				  source.src = elm.getAttribute('data-val');

				  audio.load(); //call this to just preload the audio without playing
				  audio.play(); //call this to play the song right away
				};
			}
		

			$('.play-btn').on('click', function(){
				$('#audiooption').slideToggle();
			});
			
			$('#audiooption li').on('click', function(){
				$('#audiooption').slideUp();
				$('.play-btn').text($(this).text());
			});
		},

		aboutDoctorHt: function() {

			$('.scroll-target').on('click', function(e){
				e.preventDefault();
				var getLink = $(this).data('target');
				ezone.commonFunctions.scrollTarget($('#'+getLink));
			});


			var getHT = $('.about-doctor__left').height();
			if($(window).width() > 992){
				$('.college-list__content').css('height', getHT-50);
			}	
		},

		SliderAnimations : function () {
			
			
			var autoSwap = setInterval( swap, 3500);

			$('.carousel ul').hover(
			  function () {
			    clearInterval(autoSwap);
			}, 
			  function () {
			   autoSwap = setInterval( swap,3500);
			});

			var items = [];
			var startItem = 1;
			var position = 0;
			var itemCount = $('.carousel li.items').length;
			var leftpos = itemCount;
			var resetCount = itemCount;

			$('.carousel li.items').each(function(index) {
			    items[index] = $(this).text();
			});

			function swap(action) {
			  var direction = action;
			  
			  if(direction == 'counter-clockwise') {
			    var leftitem = $('.left-pos').attr('id') - 1;
			    if(leftitem == 0) {
			      leftitem = itemCount;
			    }
			    
			    $('.right-pos').removeClass('right-pos').addClass('back-pos');
			    $('.main-pos').removeClass('main-pos').addClass('right-pos');
			    $('.left-pos').removeClass('left-pos').addClass('main-pos');
			    $('#'+leftitem+'').removeClass('back-pos').addClass('left-pos');
			    
			    startItem--;
			    if(startItem < 1) {
			      startItem = itemCount;
			    }
			  }
			  
			  if(direction == 'clockwise' || direction == '' || direction == null ) {
			    function pos(positionvalue) {
			      if(positionvalue != 'leftposition') {
			        position++;
			        
			        if((startItem+position) > resetCount) {
			          position = 1-startItem;
			        }
			      }
			    
			      if(positionvalue == 'leftposition') {
			        position = startItem - 1;
			      
			        if(position < 1) {
			          position = itemCount;
			        }
			      }
			   
			      return position;
			    }  
			  
			   $('#'+ startItem +'').removeClass('main-pos').addClass('left-pos');
			   $('#'+ (startItem+pos()) +'').removeClass('right-pos').addClass('main-pos');
			   $('#'+ (startItem+pos()) +'').removeClass('back-pos').addClass('right-pos');
			   $('#'+ pos('leftposition') +'').removeClass('left-pos').addClass('back-pos');

			    startItem++;
			    position=0;
			    if(startItem > itemCount) {
			      startItem = 1;
			    }
			  }
			}

			$('#next').click(function() {
			  swap('clockwise');
			});

			$('#prev').click(function() {
			  swap('counter-clockwise');
			});

			$('li').click(function() {
			  if($(this).attr('class') == 'items left-pos') {
			     swap('counter-clockwise'); 
			  }
			  else {
			    swap('clockwise'); 
			  }
			});
			
		},

		dashboardPage : function() {
			$('.re-yes-no input').on('change', function() {
				if($(this).data('value') != 'yes') {
					$('.re-schedule-form').hide();
				}else{
					$('.re-schedule-form').show();
				}
			});

			$('.tab-nav .tab-menu').on('click', function(){
				var getID = $(this).data('name');
				$('.tab-wrapper .tab-content').hide();	
				$('.tab-nav .tab-menu').removeClass('active');
				$(this).addClass('active');
				$('#'+getID).show();
			});
		},
	
		
		orientation : function () {
			Timer : false,
				clearTimeout(this.Timer);
				this.Timer = setTimeout(function () {
					Winwidth = window.innerWidth;
					Winheight = window.innerHeight;
					CurrentBreakpoint : 'Desktop',
					DeviceType = {
						DESKTOP : 'Desktop',
						MOBILE : 'Mobile'
					};
					CurrentDevice = DeviceType.DESKTOP;

					if (Winwidth >= 992) {
						CurrentDevice = DeviceType.DESKTOP;
						/*} else if (Winwidth > 990 && Winwidth < 1024) {
						CurrentDevice = DeviceType.TABLET;*/
					} else if (Winwidth < 992) {
						CurrentDevice = DeviceType.MOBILE;
					}

					ezone.commonFunctions.CurrentBreakpoint = CurrentDevice;

					console.log(CurrentDevice);
				}, 10);
		},

		init : function () {	
			ezone.commonFunctions.orientation();
			ezone.commonFunctions.UserAgentCode();
			ezone.commonFunctions.SliderAnimations();
			ezone.commonFunctions.hemMenu();
			ezone.commonFunctions.loginPopup();
			ezone.commonFunctions.homepageAudio();
			ezone.commonFunctions.aboutDoctorHt();
			ezone.commonFunctions.dashboardPage();
		}
	}

	$(window).on('resize', function () {
		ezone.commonFunctions.orientation();
		ezone.commonFunctions.aboutDoctorHt();
	});

	$(document).ready(function () {		
		ezone.commonFunctions.init();		
	});

	$(window).scroll(function () {
	
	});

	$(window).on('load', function(){

		//$('#audiooption li').eq(0).trigger('click');
		
	});
	

}).call(ezone.commonFunctions, window.jQuery);
